# SPICI Sleep Detection

## Short Introduction
> The driver is <br>
> :tired_face: tired<br>
> :sleepy: sleepy<br>
> :sleeping: sleeping<br>
> __SPICI Sleep Detection__ send <br>
> :warning: warning<br>
> before... <br>
> :skull: accident <br>

The function is detecting the eye lid status.<br>
It will send a warning signal, if the eyes are closed for more than 5 seconds.
<br>
## System Setup
The software was developed and tested via: <br>
__Hardware:__
+ Raspberry Pi 4 - 4GB <br>
+ Raspberry Pi OS ( 32 bit) with desktop - Debian Buster <br>
+ Release date: 2020-05-27 -  link: https://www.raspberrypi.org/downloads/raspberry-pi-os/ <br>
+ RPI Camera v2.1 (8MP) - gpu_mem=150<br>

__Software:__ 
+ opencv 4.4 - link: https://github.com/opencv <br>

## Install Instruction
sudo apt update && sudo apt upgrade  <br>
cd ~/ <br>
mkdir spici <br>
cd spici <br>
git clone https://gitlab.com/ToolboxBodensee/spici/spici_sleep_detection.git <br>
cd spici_sleep_detection/SW/src <br>
./spici_sleep_detection <br>

## Install Opencv4
First check, if opencv is alreadyinstalled: <br>
pkg-config --modversion opencv4 <br>
pkg-config opencv4 --libs <br>
<br>
Opencv4 need a lot of side packages and some changes like... <br>
sudo apt -y remove x264 libx264-dev <br>

__Install dependencies__  <br>
sudo apt -y install build-essential checkinstall cmake pkg-config yasm <br>
sudo apt -y install git  <br>
sudo apt-get install gfortran <br>
sudo apt -y install libjpeg8-dev libjasper-dev libpng12-dev <br>
sudo apt -y install libtiff5-dev <br>
sudo apt -y install libtiff-dev <br>
sudo apt -y install libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev <br>
sudo apt -y install libxine2-dev libv4l-dev <br>
cd /usr/include/linux <br>
sudo ln -s -f ../libv4l1-videodev.h videodev.h <br>
cd $cwd <br>
sudo apt -y install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev <br>
sudo apt -y install libgtk2.0-dev libtbb-dev qt5-default <br>
sudo apt -y install libatlas-base-dev <br>
sudo apt -y install libmp3lame-dev libtheora-dev <br>
sudo apt -y install libvorbis-dev libxvidcore-dev <br>
sudo apt -y install libopencore-amrnb-dev libopencore-amrwb-dev <br>
sudo apt -y install libavresample-dev <br>
sudo apt -y install x264 v4l-utils <br>
__Optional dependencies__  <br>
sudo apt -y install libprotobuf-dev protobuf-compiler <br>
sudo apt -y install libgoogle-glog-dev libgflags-dev <br>
sudo apt -y install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen <br>

_INFO: Helpfull Install Tutorials_
https://cv-tricks.com/installation/opencv-4-1-ubuntu18-04/ <br>
https://www.learnopencv.com/install-opencv-3-4-4-on-ubuntu-16-04/ <br>

__OpenCV__  <br>
cd ~/ <br>
mkdir opencv_root <br>
cd opencv_root <br>
git clone https://github.com/opencv/opencv.git <br>
(optional): git clone https://github.com/opencv/opencv_contrib.git <br>
(optional): git clone https://github.com/opencv/opencv_extra.git <br>
cd opencv <br>
git checkout 4.4.0 <br>
cd .. <br>
<br>
(optional): opengl support <br>
(optional): sudo apt-get install libgtkglext1-dev <br>
(optional): sudo apt-get install libqt4-opengl-dev <br>
(optional): FLAG->  -D WITH_OPENGL=ON <br>
<br>
mkdir build <br>
cd build <br>
```
cmake -D CMAKE_BUILD_TYPE=RELEASE \ 
    -D CMAKE_INSTALL_PREFIX=/usr/local \ 
    -D INSTALL_C_EXAMPLES=ON \ 
    -D OPENCV_GENERATE_PKGCONFIG=ON \ 
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_root/opencv_contrib/modules \ 
    -D WITH_OPENCL=OFF \
    -D BUILD_EXAMPLES=ON ..

    All in one line to copy it:
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D OPENCV_GENERATE_PKGCONFIG=ON -D OPENCV_EXTRA_MODULES_PATH=~/opencv_root/opencv_contrib/modules -D WITH_OPENCL=OFF -D BUILD_EXAMPLES=ON ..
```
make -j$(nproc) <br>
sudo make install <br> 

## Opencv4 Installation Check
pkg-config --modversion opencv4 <br>
pkg-config --cflags opencv4 <br>
pkg-config --libs opencv4 <br>
see pkg-config --help for more options <br>
<br>
## Install MQTT
sudo apt-get install  mosquitto mosquitto-clients <br>
## MQTT Installation Check
terminal 1: mosquitto_sub -h localhost -t "test" <br>
terminal 2: mosquitto_pub -h localhost -t "test" -m "spici_sleep_detection" <br>
<br>
## Install Bluetooth (incl. autostart via serial connection to OBDII-dongle)
sudo apt-get install bluetooth bluez bluez-tools bluez-firmware blueman <br>
sudo nano /etc/bluetooth/rfcomm.conf <br>
<br>

```
rfcomm0 {
    # Automatically bind the device at startup
    bind yes;
    # Bluetooth address of the device
    device 00:1D:A5:68:98:8A;
    # RFCOMM channel for the connection
    channel 1;
    # Description of the connection
    comment "SPICI OBDII Connection";
} 
```

 <br>
#bluetooth autostart
Edit: sudo nano /etc/bluetooth/main.conf <br>
after [General] add .. <br>
DisablePlugins = pnat <br>
 <br>
Edit: sudo nano /etc/systemd/system/bluetooth.target.wants/bluetooth.service <br>
below [service] <br>
change: <br>
ExecStart=/usr/lib/bluetooth/bluetoothd <br>
to: <br>
ExecStart=/usr/lib/bluetooth/bluetoothd --compat --noplugin=sap <br>
 <br>
Edit: sudo nano /etc/rc.local <br>
add following lines ... before the >exit 0< <br>
sudo rfcomm bind hci0 00:1D:A5:68:98:8A 1 <br>
sudo chmod 777 /var/run/sdp <br>
sudo chmod a+rw /dev/rfcomm0 <br>
sudo chmod +x /home/pi/spici/spici_sleep_detection/obd/spiciobd.py <br>
sudo /home/pi/spici/spici_sleep_detection/obd/spiciobd.py & <br>
 <br>
Test that the python script is running after autostart: <br>
ps -ef | grep python <br>
 <br>
## have fun ;-)


