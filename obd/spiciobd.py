#!/usr/bin/env python
# SPICI OBD Tool
# autor: Joerg Angermayer
# date: 26.10.2020
# release: v1.10
# 
# target: grab data via bluetooth OBD interface(EM327) and store it inside ramdev.
# precondition: serial bluetooth connection to OBD interface(EM327) must be available.
# add data to MQTT (Mosquitto must be installed)

import serial
import time
import paho.mqtt.client as mqtt

client = mqtt.Client()

msg_info_enabled = 1;

ramEngineCoolantTemp = 0 #PID 0x05
ramRpm = 0 #PID 0x0C
ramSpeed = 0 #PID 0x0D
ramGasPedalLevelD = 0 # PID 49 Gaspedalstellung D  
#PID 4A Gaspedalstellung E 

ramEngineruntimeAfterStart = 0 #PID 1F - 2Byte 1Sec/Bit
ramECUvoltage = 0 #PID 0x42 - input voltage - Batterie ? (0.001V pro Bit - 2 Byte)
ramEnvironmentTemp = 0 #PID 0x46 (-40 bis ...)

#PID 4B Gaspedalstellung F ---Not supported
ramFuellLevel = 0 #PID 0x2F - 0-100%   ---Not supported
ramOilTemp = 0 #PID 0x5C (-40 bis ...) ---Not supported
ramPid67EngineCoolantTemp = 0 # Pid 67 ---Not supported

ramPid11AbsoluteDrosselklappenstellung = 0 # PID 11 Absolute Drosselklappenstellung
ramPid11RelativeDrosselklappenstellung = 0 # PID 45 Relative Drosselklappenstellung

### MQTT functions
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    
def mqtt_init():
    
    client.on_connect = on_connect
    client.connect("localhost", 1883, 60)
    client.loop_start()
    

### OBD common functions
def em327at(serialconnection, request, delaytime):
    serialconnection.write(bytes('at' + request + '\r\n'))
    time.sleep(delaytime) 
    response = serialconnection.readline()
    return response
    
def reqObd(serialconnection, request, delaytime):
    ser.write(bytes(request + '\r\n'))
    time.sleep(delaytime)
    response = serialconnection.readline()
    return response

### ODB read functions
def grabEngineCoolantTemp():
    try:
        obdEngineCoolantTemp = reqObd(ser, '0105', 0)
        #print "obdEngineCoolantTemp: ",obdEngineCoolantTemp
        listEngineCoolantTemp = list(obdEngineCoolantTemp)
        listEngineCoolantTemp[0:5] = []
        strEngineCoolantTemp = "".join(listEngineCoolantTemp) 
        strEngineCoolantTemp = strEngineCoolantTemp[:-4]
        EngineCoolantTemp = int(strEngineCoolantTemp,16) - 40    
    except:        
        EngineCoolantTemp = 0
        if(msg_info_enabled == 1):
            print("Be sure that the ignition is on, please.")
            print("Bitte stellen Sie sicher das die Zuendung an ist.")
            msg_info_enabled = 0
    finally:
        return EngineCoolantTemp
        
def grabRPM():
    try:
        odbrpm = reqObd(ser, '010C', 0)
        listRpm = list(odbrpm)
        listRpm[0:6] = []
        p = listRpm.index(" ")
        del(listRpm[p])
        strRpm = "".join(listRpm)  
        strRpm = strRpm[:-4]
        intRpm = int(strRpm, 16) / 4
    except:
        intRpm = 0
    finally:
        return intRpm 

def grabSpeed():
    try:
        odbSpeed = reqObd(ser, '010D', 0)
        listSpeed = list(odbSpeed)
        listSpeed[0:6] = []
        p = listSpeed.index(" ")
        del(listSpeed[p])
        strSpeed = "".join(listSpeed)  
        strSpeed = strSpeed[:-4]
        speed = int(strSpeed, 16) / 1.609
    except:
        speed = 0
    finally:
        return speed

def grabGasPedalLevelD():
    try:
        obdGasPedalLevel = reqObd(ser, '0149', 0) # GasPedalLevel D
        listGasPedalLevel = list(obdGasPedalLevel)
        listGasPedalLevel[0:6] = []
        strGasPedalLevel = "".join(listGasPedalLevel)  
        strGasPedalLevel = strGasPedalLevel[:-4]    
        GasPedalLevel = int(strGasPedalLevel,16) * 100.0 / 255.0
        GasPedalLevel = round(GasPedalLevel,2)
    except:
        GasPedalLevel = 0
    finally:        
        return GasPedalLevel

def grabEngineruntimeAfterStart():
    try:
        obdEngineruntimeAfterStart = reqObd(ser, '011F', 0)
        listEngineruntimeAfterStart = list(obdEngineruntimeAfterStart)
        listEngineruntimeAfterStart[0:6] = []
        p = listEngineruntimeAfterStart.index(" ")
        del(listEngineruntimeAfterStart[p])
        strEngineruntimeAfterStart = "".join(listEngineruntimeAfterStart)  
        strEngineruntimeAfterStart = strEngineruntimeAfterStart[:-4]
        EngineruntimeAfterStart = int(strEngineruntimeAfterStart,16)    
    except:      
        EngineruntimeAfterStart = 0
    finally:
        return EngineruntimeAfterStart

def grabECUvoltage():
    try:
        obdECUvoltage = reqObd(ser, '0142', 0)
        listECUvoltage = list(obdECUvoltage)
        listECUvoltage[0:6] = []
        p = listECUvoltage.index(" ")
        del(listECUvoltage[p])
        strECUvoltage = "".join(listECUvoltage)  
        strECUvoltage = strECUvoltage[:-4]
        ECUvoltage = int(strECUvoltage,16) / 1000.0
        ECUvoltage = round(ECUvoltage,2)
    except:
        ECUvoltage = 0
    finally:
        return ECUvoltage

def grabEnvironmentTemp():
    try:
        obdEnvironmentTemp = reqObd(ser, '0146', 0)
        listEnvironmentTemp = list(obdEnvironmentTemp)
        listEnvironmentTemp[0:5] = []
        strEnvironmentTemp = "".join(listEnvironmentTemp) 
        strEnvironmentTemp = strEnvironmentTemp[:-4]
        EnvironmentTemp = int(strEnvironmentTemp,16) - 40
    except:
        EnvironmentTemp = 0
    finally:
        return EnvironmentTemp


###MAIN###########################
### MQTT ###    
mqtt_init()

### OBD ###
ser = serial.Serial()
ser.port = '/dev/rfcomm0'
ser.baudrate = 38400
ser.timeout=1 #timeout after 1 sec.
print(ser)
print("open Port: ...")
try:
    ser.open() 
except:
    if(msg_info_enabled == 1):
        print("no serial connection via rfcomm0(bluetooth) .")
        print("keine serielle Verbindung mit rfcomm0(bluetooth).")
        msg_info_enabled = 0
    
    
try:
    print(em327at(ser,'z',3))   #reset EM327
    print(em327at(ser,'E0',1))  #disable echo
    #print(em327at(ser,'ign',1))#ign status
    #print(em327at(ser,'rv',1)) #voltage at EM327
    #print(em327at(ser,'al',1)) #Allow Long (>7 byte) messages to 
except:
    if(msg_info_enabled == 1):
        print("EM327 is not reachable - proof bluetooth connection, please.")
        print("EM327 ist nicht erreichbar - bitte bluetooth-Verbindung pruefen.")
        msg_info_enabled = 0

### OBD 
try: 
    print(" --- support check 0100:")
    response0100 = reqObd(ser, '0100', 5) # enable protokoll
    print("\n:",response0100)
except:
    if(msg_info_enabled == 1):
        print(" ---connection failed---")
        msg_info_enabled = 0
while 1:
    ramEngineCoolantTemp = grabEngineCoolantTemp()
    ramRpm = grabRPM()
    ramSpeed = grabSpeed()
    ramGasPedalLevelD = grabGasPedalLevelD()
    ramEngineruntimeAfterStart = grabEngineruntimeAfterStart()
    ramECUvoltage = grabECUvoltage()
    ramEnvironmentTemp = grabEnvironmentTemp()    
    '''
    print "\n --- > OBD <---"
    print "EngineCoolantTemp: ",ramEngineCoolantTemp," celsius"
    print "RPM: ",ramRpm," 1/min"
    print "Speed: ",ramSpeed," km/h"
    print "GasPedalLevel D: ",ramGasPedalLevelD," %"
    print "EngineruntimeAfterStart: ",ramEngineruntimeAfterStart," sec."
    print "Battery: ",ramECUvoltage," voltage"
    print "EnvTemp: ",ramEnvironmentTemp," celsius"
    '''
    try:
        # Filename to write
        filename = "/var/www/html/ramdev/obd.dat"
        obdfile = open(filename, 'w')
        obdfile.write(str(ramEngineCoolantTemp)+';'+str(ramRpm)+';'+str(ramSpeed)+';'+str(ramGasPedalLevelD)+';'+str(ramEngineruntimeAfterStart)+';'+str(ramECUvoltage)+';'+str(ramEnvironmentTemp)+'\n')
        obdfile.close()
    except:
        if(msg_info_enabled == 1):
            print("not possible to write the file: /var/www/html/ramdev/obd.dat")
            msg_info_enabled = 0
            time.sleep(5)# wait 2 seconds
        
    ### MQTT
    client.publish("SPICI1/OBD/EngineCoolantTemp", ramEngineCoolantTemp)
    client.publish("SPICI1/OBD/RPM", ramRpm)
    client.publish("SPICI1/OBD/Speed", ramSpeed)
    client.publish("SPICI1/OBD/GasPedalLevel", ramGasPedalLevelD)
    client.publish("SPICI1/OBD/EngineruntimeAfterStart", ramEngineruntimeAfterStart)
    client.publish("SPICI1/OBD/Battery", ramECUvoltage)
    client.publish("SPICI1/OBD/EnvTemp", ramEnvironmentTemp)
    time.sleep(0.300)# sleep to reduce the cpu work time - increase idele time
    #print("sleep 0.500 sec.")

print("Close serial connection to OBD")
try:
    ser.close()  # close serial connection
except:
    if(msg_info_enabled == 1):
        print("Closing from serial connection failed.")
        msg_info_enabled = 0





