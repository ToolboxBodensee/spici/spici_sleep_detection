function refreshImg(element, refreshtime) {
    setTimeout(function() {
        element.src = element.src.split('?')[0] + '?' + new Date().getTime();
        refreshImg(element,refreshtime);
        }, refreshtime); //refresh every 1000ms
    }
