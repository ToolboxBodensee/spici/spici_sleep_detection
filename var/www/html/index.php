<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8">
                <meta http-equiv="Content-Language" content="de">
                <meta http-equiv="refresh" content="1">               
                <script src="refreshImg.js"> </script>
        </head>
        <body bgcolor="#00002b">
        <font color="white" face="Georgia, Arial">
                <center>
                <h1>SPICI - Sleep Detection</h1> <br> 
                </center>
                <font color="black">
                <center><img src="ramdev/spici_sleep_detection.jpg" border=10 alt="" width="640" height="480" id="imglive" onload="refreshImg(this,333)" style="float:right; margin-left: 15px;"></center> <br>                                                
                </font>
                <font color="white">
                <?php                                
                $datenlaenge = filesize("/var/www/html/ramdev/spici_sleep_detection.dat");
                $datei = fopen("/var/www/html/ramdev/spici_sleep_detection.dat","r");
                $data00 = fgets($datei,intval($datenlaenge)+1);
                fclose($datei);  
  
                //Daten auswerten
                $data_array = explode(";",$data00);
                $eyeopen_status = $data_array[0];
                $sleepDetectionStatus = $data_array[1];
                $driverBrigthnessStatus = $data_array[2];
                $sleepDetectionTimeCounter = $data_array[3];
                $delta_EyeLeft = $data_array[4];
                $delta_EyeRight = $data_array[5];                                      
                ?>
                <br>
                <!-- Sleep Detection Status: 0 = nothing detected; 1 = Sleeping Detected; 2 = Awake -->
                <b>Sleep Detection Status: </b><br>
                <?php if ($sleepDetectionStatus == 0): ?>
                        Nothing detected<br>
                <?php else: ?>
                        <?php if ($sleepDetectionStatus == 1): ?>
                                Sleeping<br>
                        <?php else: ?>
                                Awake<br>
                        <?php endif ?>
                <?php endif ?>
                
                <!-- Eye open status 0 = nothing detectd ; 1 = open; 2 = closed -->
                <b>Eye Open Status: </b> <br>
                <?php if ($eyeopen_status == 0): ?>
                        Nothing detected<br>
                        <?php 
                        $delta_EyeLeft = 0;
                        $delta_EyeRight = 0;
                        else: ?>
                        <?php if ($eyeopen_status == 1): ?>
                                open<br>
                        <?php else: ?>
                                closed<br>
                                <?php
                                $delta_EyeLeft = 0;
                                $delta_EyeRight = 0;
                                endif ?>
                <?php endif ?>
                
                <b>Sleep Detection Timer: </b> <br><?php echo $sleepDetectionTimeCounter;?> second/s<br> 
                
                <b>Eye Lid Delta:</b><br>
                <table>
                <tr><th align="left">Left:</th><th align="right" style="font-weight:normal"><?php echo $delta_EyeLeft;?> pixel</th></tr> 
                <tr><th align="left">Right:</th><th align="right" style="font-weight:normal"><?php echo $delta_EyeRight;?> pixel</th></tr> 
                <table>
                <!--      
                <br>
                <b>Brigthness from driver face: </b><br><?php echo $driverBrigthnessStatus;?> digits from 255(white)<br>                
                <b>Ctrl:</b><br>
                fpress <b>m</b> to control the MQTT debug output <br>
                press <b>f</b> to control the facemarker visibility<br>
                <br>
                -->
                <?php
                //OBD - On Board Diagnose              
                $obddatenlaenge = filesize("/var/www/html/ramdev/obd.dat");
                $obddatei = fopen("/var/www/html/ramdev/obd.dat","r");
                $dataobd = fgets($obddatei,intval($obddatenlaenge)+1);
                fclose($obddatei);  
  
                //Daten einlesen
                $data_obdarray = explode(";",$dataobd);
                $obdEngineCoolantTemp = $data_obdarray[0];
                $obdRPM = $data_obdarray[1];
                $obdSpeed = $data_obdarray[2];
                $obdGasPedal = $data_obdarray[3];
                $obdEngineruntimeAfterStart = $data_obdarray[4];
                $obdBattery = $data_obdarray[5];     
                $obdEnv = $data_obdarray[6];                                   
                ?>                                            

                <br><b>OBD - On Board Diagnose - Data: </b> <br>
                <?php if ($obdBattery == 0): ?>                        
                        --- Data not available ---<br>
                        proof the connection, please.<br>
                <?php else: ?>
                        <b>EngineCoolantTemp: </b><?php echo $obdEngineCoolantTemp;?> °C<br> 
                        <b>Environment temperature: </b><?php echo $obdEnv;?> °C<br>
                        <b>GasPedal: </b><?php echo $obdGasPedal;?> %<br>                        
                        <b>Speed: </b><?php echo $obdSpeed;?> [km/h]<br> 
                        <b>RPM: </b><?php echo $obdRPM;?> [1/min]<br>
                        <b>EngineRuntimeAfterStart: </b><?php echo $obdEngineruntimeAfterStart;?> sec.<br> 
                        <b>Battery: </b><?php echo $obdBattery;?> V<br>                                                  
                <?php endif ?>
                </font>
        </body>
</html>

