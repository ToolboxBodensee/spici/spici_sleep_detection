<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8">
                <meta http-equiv="Content-Language" content="de">
                <meta http-equiv="refresh" content="1">               
        </head>
        <body bgcolor="#00002b">
        <font color="white" face="Georgia, Arial">
                <center>
                <h1>SPICI - Sleep Detection</h1> <br> 
                </center>
                <center>
                <?php                                
                $datenlaenge = filesize("/var/www/html/ramdev/spici_sleep_detection.dat");
                $datei = fopen("/var/www/html/ramdev/spici_sleep_detection.dat","r");
                $data00 = fgets($datei,intval($datenlaenge)+1);
                fclose($datei);  
  
                //Daten auswerten
                $data_array = explode(";",$data00);
                $eyeopen_status = $data_array[0];
                $sleepDetectionStatus = $data_array[1];
                $driverBrigthnessStatus = $data_array[2];
                $sleepDetectionTimeCounter = $data_array[3];
                $delta_EyeLeft = $data_array[4];
                $delta_EyeRight = $data_array[5];                                      
                ?>
                <br>
                <!-- Sleep Detection Status: 0 = nothing detected; 1 = Sleeping Detected; 2 = Awake -->
                <b>Sleep Detection Status: </b><br>
                <?php if ($sleepDetectionStatus == 0): ?>
                        Nothing detected<br>
                <?php else: ?>
                        <?php if ($sleepDetectionStatus == 1): ?>
                                Sleeping<br>
                        <?php else: ?>
                                Awake<br>
                        <?php endif ?>
                <?php endif ?>
                <br>                
                <!-- Eye open status 0 = nothing detectd ; 1 = open; 2 = closed -->
                <b>Eye Open Status: </b> <br>
                <?php if ($eyeopen_status == 0): ?>
                        Nothing detected<br>
                        <?php 
                        $delta_EyeLeft = 0;
                        $delta_EyeRight = 0;
                        else: ?>
                        <?php if ($eyeopen_status == 1): ?>
                                open<br>
                        <?php else: ?>
                                closed<br>
                                <?php
                                $delta_EyeLeft = 0;
                                $delta_EyeRight = 0;
                                endif ?>
                <?php endif ?>
                <br>
                <b>Sleep Detection Timer: </b> <br><?php echo $sleepDetectionTimeCounter;?> second/s<br> 
                
                <br>
                <b>Eye Lid Delta:</b><br>
                <table>
                <tr><th align="left">Left:</th><th align="right" style="font-weight:normal"><?php echo $delta_EyeLeft;?> pixel</th></tr> 
                <tr><th align="left">Right:</th><th align="right" style="font-weight:normal"><?php echo $delta_EyeRight;?> pixel</th></tr> 
                <table>
                 </center>
                </font>
        </body>
</html>

