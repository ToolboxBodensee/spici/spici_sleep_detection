<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8">
                <meta http-equiv="Content-Language" content="de">
                <meta http-equiv="refresh" content="1">               
        </head>
        <body bgcolor="#00002b">
        <font color="white" face="Georgia, Arial">
                <center>
                <h1>OBD II Detection</h1> <br> 
                </center>

                <center>
                <?php
                //OBD - On Board Diagnose              
                $obddatenlaenge = filesize("/var/www/html/ramdev/obd.dat");
                $obddatei = fopen("/var/www/html/ramdev/obd.dat","r");
                $dataobd = fgets($obddatei,intval($obddatenlaenge)+1);
                fclose($obddatei);  
  
                //Daten einlesen
                $data_obdarray = explode(";",$dataobd);
                $obdEngineCoolantTemp = $data_obdarray[0];
                $obdRPM = $data_obdarray[1];
                $obdSpeed = $data_obdarray[2];
                $obdGasPedal = $data_obdarray[3];
                $obdEngineruntimeAfterStart = $data_obdarray[4];
                $obdBattery = $data_obdarray[5];     
                $obdEnv = $data_obdarray[6];                                   
                ?>                                            

                <br><b>OBD II - On Board Diagnose - Data: </b> <br><br>
                <?php if ($obdBattery == 0): ?>                        
                        --- Data not available ---<br>
                        proof the connection, please.<br>
                <?php else: ?>
                        <center>             
                        <table>                
                        <tr><th align="left"><b>EngineCoolantTemp: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdEngineCoolantTemp;?> [°C] </th></tr>
                        <tr><th align="left"><b>Environment temperature: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdEnv;?> [°C] </th></tr>
                        <tr><th align="left"><b>GasPedal: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdGasPedal;?> [%] </th></tr>
                        <tr><th align="left"><b>Speed: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdSpeed;?> [km/h] </th></tr>
                        <tr><th align="left"><b>RPM: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdRPM;?> [1/min] </th></tr>
                        <tr><th align="left"><b>EngineRuntimeAfterStart: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdEngineruntimeAfterStart;?> [sec.] </th></tr>
                        <tr><th align="left"><b>Battery: </b></th><th align="left" style="font-weight:normal"> <?php echo $obdBattery;?> [V.] </th></tr>
                        <table>
                        </center>                                         
                <?php endif ?>
                </center>
                </font>
        </body>
</html>

