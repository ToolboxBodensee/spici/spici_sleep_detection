//============================================================================
// Name        : spici_sleep_detection.cpp
// Author      : J.Angermayer
// Release     : 0.3
// Copyright   : opensource
// Description : Developet and tested on RPI4 - 4GB - gpu_mem=150 - Pi OS (32bit) Release 2020-05-27
//============================================================================

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/face.hpp"
using namespace cv::face;

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
using namespace cv;

#include <time.h>
#include <sys/stat.h>
#include <string>
#include <fstream>

#define ENABLED_FRAME_WIDTH  640
#define ENABLED_FRAME_HEIGHT 480

FILE *file_ssd;
static int eyeopen_status = 0; // 0 = nothing detectd ; 1 = open; 2 = closed
static int sleepDetectionTimeCounter = 0;
static int sleepDetectionStatus = 0; // 0 = nothing detectd ; 1 = Sleeping Detected; 2 = Awake
static int driverBrigthnessStatus = 0;
static int delta_EyeLeft = 0;
static int delta_EyeRight = 0;
static int lasttimeinsec = 0;
const int constSleepTime = 5;
const int constSleepPixelLimit = 18;

static bool bdebugMQTT = false;
static bool bdebugDrawFaceMarks = false;
Scalar colorFaceMarks(0,210,210); 

int detect_brigthness(cv::Mat img,int x1,int y1,int x2,int y2);

//store data to ramdev
std::string imgfile_spici_sleep_detection("/var/www/html/ramdev/spici_sleep_detection.jpg");

bool exists_file (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}


int detect_brigthness(cv::Mat img,int x1,int y1,int x2,int y2){
  int ret = -1;  
  
  int mid_x = ((x2-x1)/2)+x1;
  int size_x = 10;
  int size_y = 20;
  int offset_y = 30;
  
  cv::Rect roi;
  roi.x = mid_x-size_x;
  roi.y = y1-offset_y-size_y;
  roi.width = 20;
  roi.height = 20;
  if((roi.x > roi.width) && (roi.y > roi.height )){
    Mat img_brigthness = img(roi);
    Mat hsv;
    cv::cvtColor(img_brigthness, hsv, cv::COLOR_BGR2HSV);
    const auto result = cv::mean(hsv);
    // 0 = hue
    // 1 = saturation
    // 2 = brigthness
    ret = result[2];
  }
  
  return(ret);
}

void faceDetector(const Mat& image,  std::vector<Rect> &faces,  CascadeClassifier &face_cascade) {
  Mat gray;
  // The cascade classifier works best on grayscale images
  if (image.channels() > 1) {
    cvtColor(image, gray, COLOR_BGR2GRAY);
  } else {
    gray = image.clone();
  }
  // Histogram equalization generally aids in face detection
  equalizeHist(gray, gray);
  faces.clear();
  // Run the cascade classifier
  face_cascade.detectMultiScale(
    gray,
    faces,
    1.4, // pyramid scale factor
    3,   // lower thershold for neighbors count
         // here we hint the classifier to only look for one face
    CASCADE_SCALE_IMAGE + CASCADE_FIND_BIGGEST_OBJECT);
}

int main( )
{
    time_t curtime;
    struct tm *loctime;
    //init lasttimer
    curtime = time (NULL);
    loctime = localtime (&curtime);
    lasttimeinsec = loctime->tm_sec;
  
	char cascade_name[]="./data/haarcascade_frontalface_default.xml";
	char facemark_filename[]="./data/lbfmodel.yaml";	
	char cwd[256];

	if (getcwd(cwd, sizeof(cwd)) == NULL)
	  perror("getcwd() error");
	else
	  printf("current working directory is: %s\n", cwd);

	///////////////////
	vector<Point3f> objectPoints {
	  {8.27412, 1.33849, 10.63490},    //left eye corner
	  {-8.27412, 1.33849, 10.63490},   //right eye corner
	  {0, -4.47894, 17.73010},         //nose tip
	  {-4.61960, -10.14360, 12.27940}, //right mouth corner
	  {4.61960, -10.14360, 12.27940},  //left mouth corner
	};
	vector<int> landmarksIDsFor3DPoints {45, 36, 30, 48, 54}; // 0-index
	///////////////////
	vector<Point3f> objectPointsForReprojection {
	  objectPoints[2],                   // tip of nose
	  objectPoints[2] + Point3f(0,0,15), // nose and Z-axis
	  objectPoints[2] + Point3f(0,15,0), // nose and Y-axis
	  objectPoints[2] + Point3f(15,0,0)  // nose and X-axis
	};

   // Load Face cascade (.xml file)
       CascadeClassifier face_cascade;

       face_cascade.load(cascade_name);
       if(!face_cascade.load(cascade_name))
       {
         cerr<<"Error Loading XML file"<<endl;
         return 0;
       }
       //const string facemark_filename = "data/lbfmodel.yaml";
       Ptr<Facemark> facemark = createFacemarkLBF();
       facemark->loadModel(facemark_filename);
       cout << "Loaded facemark LBF model" << endl;

       /////////////////
 VideoCapture capture(0);
    if (!capture.isOpened())
    throw "Error when reading file";
    //RPI cam v2 
    capture.set(CAP_PROP_FRAME_WIDTH,ENABLED_FRAME_WIDTH);capture.set(CAP_PROP_FRAME_HEIGHT,ENABLED_FRAME_HEIGHT);
        
    //cv::namedWindow("Detected Face");
    //cv::moveWindow("Detected Face", 50,50);
    
    sleepDetectionStatus = 0;
    if(bdebugMQTT == true){
      system("mosquitto_pub -d -t spici_sleep_Detection -m  \"STATUS:Nothing detected\" ");
    }
    
    while(1){
       Mat img;
       capture >> img;
       if (img.empty())
       break;

      // Detect faces
      std::vector<Rect> faces;
      face_cascade.detectMultiScale( img, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );
      // Check if faces detected or not
      if (faces.size() != 0) {
        vector<vector<Point2f> > shapes;

        if (facemark->fit(img, faces, shapes)) {    	        
			vector<Point2f> points2d_A;
			vector<Point2f> points2d_B;
			vector<Point2i> point2i_A;
			vector<Point2i> point2i_B;
			vector<cv::Point> p_A;
			vector<cv::Point> p_B;

			vector<cv::Point> p_C;
			vector<cv::Point> p_D;

			p_A.push_back(shapes[0][37]);
			p_B.push_back(shapes[0][41]);

			static int EyeLeftStatus = 0;
			static int EyeRightStatus = 0;
			
			int eyeLx1; int eyeLx2; int eyeLy1; int eyeLy2;
			int eyeRx1; int eyeRx2; int eyeRy1; int eyeRy2;
			int x1,x2,y1,y2,lineThickness;
			//EYE LEFT
			x1=100; x2=200; y1=100;y2=200; lineThickness = 1;
			x1=p_A[0].x;y1=p_A[0].y;
			x2=p_B[0].x;y2=p_B[0].y;
                        eyeLx1 = x1; eyeLx2 = x2; eyeLy1 = y1; eyeLy2 = y2;
						
			delta_EyeLeft = eyeLy2-eyeLy1;
			if(delta_EyeLeft < constSleepPixelLimit){EyeLeftStatus = 0;}
			if(delta_EyeLeft > constSleepPixelLimit){EyeLeftStatus = 1;}
			int eyeleftpos_x = eyeLx1;
						
			//EYE RIGHT
			p_C.push_back(shapes[0][44]);
			p_D.push_back(shapes[0][46]);
			x1=p_C[0].x;y1=p_C[0].y;
			x2=p_D[0].x;y2=p_D[0].y;
			eyeRx1 = x1; eyeRx2 = x2; eyeRy1 = y1; eyeRy2 = y2;				
			int eyerightpos_x = eyeRx1;
			
			//LOGIC
                        //face plausibility check
			int facePlausDelta = abs(eyeleftpos_x - eyerightpos_x);
			if(facePlausDelta > 100){  	
			  if(bdebugDrawFaceMarks == true){
			    //draw Face Marks
			    int index;
			    index = 0;
			    while(index<(int)faces.size()){
			      face::drawFacemarks(img, shapes[index], colorFaceMarks);
			    index++;	      
			    }//end_while
			  }//end_if		  
			  //draw eye open bars
                          Scalar colorEyeRect(0,210,0);  
			  Scalar colorEyeRect_green(0,210,0); 
			  Scalar colorEyeRect_blue(210,0,0); 
			  int EyeRectOffset = 16;
			  int EyeRectSize = 4;	
			  
			  //EYE LID Logic
			  delta_EyeRight = eyeRy2-eyeRy1;
			  if(delta_EyeRight < constSleepPixelLimit){EyeRightStatus = 0;}
			  if(delta_EyeRight > constSleepPixelLimit){EyeRightStatus = 1;}	
			  //time	
			  curtime = time (NULL);
			  loctime = localtime (&curtime);	
			  int timeinsec = loctime->tm_sec;
			  
			  if((EyeLeftStatus==0)&&(EyeRightStatus==0)){ 
			      eyeopen_status = 2;
			      colorEyeRect = colorEyeRect_blue;			      			      			      			      
			      if(timeinsec != lasttimeinsec){ 
				sleepDetectionTimeCounter++; 
				if(sleepDetectionTimeCounter >= constSleepTime){
				  sleepDetectionTimeCounter = constSleepTime;				     
				    sleepDetectionStatus = 1;
				    //driverBrigthnessStatus = 0;
				    if(bdebugMQTT == true){
				       system("mosquitto_pub -d -t \"spici_sleep_detection\" -m  \"STATUS:Sleeping\" ");
				    }
				  }
			      }			    
			  }else{
			    eyeopen_status = 1; 			    
			    driverBrigthnessStatus = detect_brigthness(img,eyeLx1,eyeLy1,eyeRx1,eyeRy1);
			    char strMqttCliBrigthness[200];
			    sleepDetectionStatus = 2;
			    system(strMqttCliBrigthness);
			    if(bdebugMQTT == true){
			      sprintf(strMqttCliBrigthness,"mosquitto_pub -d -t \"spici_sleep_detection\" -m \"STATUS:Awake; brigthness:%d\" ",driverBrigthnessStatus);
			    }
			    
			      if(timeinsec != lasttimeinsec){ 				 
				if(sleepDetectionTimeCounter > 0){
				  sleepDetectionTimeCounter--;
				}
			      }
			  }
			  lasttimeinsec = timeinsec;
			  
			  //Draw Counter			  
			  char strSleepTimer[200];
			  if(sleepDetectionTimeCounter < constSleepTime){
			    cv::rectangle(img, Point(0,0), Point(40,40), Scalar(0,0,0),-1,8,0);
			    sprintf(strSleepTimer,"%d",sleepDetectionTimeCounter);
			    cv::putText(img,strSleepTimer,cv::Point(10,30),cv::FONT_HERSHEY_TRIPLEX,1.0,Scalar(0,255,255),1);
			  }else{
			    static bool bwarn = false;
			    int txt_pos_SleepingDetected = 180;
			    if(bwarn == false){
			      bwarn = true;
			      cv::rectangle(img, Point(0,0), Point(640,40), Scalar(0,0,0),-1,8,0);
			      sprintf(strSleepTimer,"Sleeping Detected");
			      cv::putText(img,strSleepTimer,cv::Point(txt_pos_SleepingDetected,30),cv::FONT_HERSHEY_TRIPLEX,1.0,Scalar(0,0,255),1);
			    }else{
			      bwarn=false;
			      cv::rectangle(img, Point(0,0), Point(640,40), Scalar(0,0,255),-1,8,0);
			      sprintf(strSleepTimer,"Sleeping Detected");
			      cv::putText(img,strSleepTimer,cv::Point(txt_pos_SleepingDetected,30),cv::FONT_HERSHEY_TRIPLEX,1.0,Scalar(0,0,0),1);
			    }
			  }
			  			  
			  //left eye	
			  cv::rectangle(img, Point(eyeLx1-EyeRectOffset,eyeLy1), Point(eyeLx1-EyeRectOffset-EyeRectSize,eyeLy2), colorEyeRect,-1,8,0);	
			  //right eye
			  cv::rectangle(img, Point(eyeRx1+EyeRectOffset,eyeRy1), Point(eyeRx1+EyeRectOffset+EyeRectSize,eyeRy2), colorEyeRect,-1,8,0);
			  
			}// face unplausible check
        }
      }else{
         //Faces not detected
	 eyeopen_status = 0;
	 sleepDetectionStatus = 0;	  
	 //driverBrigthnessStatus = 0;
	 if(bdebugMQTT == true){
	   system("mosquitto_pub -d -t \"spici_sleep_detection\" -m  \"nothing detected\" ");	   
         }
      }
      //imshow( "Detected Face", img );   
      /*
      // encode mat to jpg and copy it to content      
      vector<uchar> buf;
      imencode(".jpg", img, buf, std::vector<int>());
      string content(buf.begin(), buf.end()); //this must be sent to the client
      cv::imwrite("/var/www/html/ramdev/spici_sleep_detection_stream.jpg",img);
      */
      cv::imwrite("/var/www/html/ramdev/spici_sleep_detection.jpg",img);	    
      file_ssd = fopen("/var/www/html/ramdev/spici_sleep_detection.dat","w+");
      fprintf(file_ssd,"%d;%d;%d;%d;%d;%d",eyeopen_status,sleepDetectionStatus,driverBrigthnessStatus,sleepDetectionTimeCounter, delta_EyeLeft, delta_EyeRight);
      fclose(file_ssd);           
      
      //Debug options
      char key = (char)waitKey(30);
      if(key == 27){ return EXIT_SUCCESS;}
      if( (key == 'm')||(key == 'M') ){ 
	  if(bdebugMQTT == true){
	    bdebugMQTT = false;
	  }else{
	    bdebugMQTT = true;
	  }
      }
      
      if( (key == 'f')||(key == 'F') ){ 
	  if(bdebugDrawFaceMarks == true){
	    bdebugDrawFaceMarks = false;
	  }else{
	    bdebugDrawFaceMarks = true;
	  }
      }
      
      //delay to reduce CPU workload
      usleep(125000);//125 ms
      //usleep(200000);//200 ms
      
   }//end while
   return 0;
}
